﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.models
{
    /// <summary>
    /// The ShortestPathsFinding class
    /// <para>finds the shortest paths between two nodes, 
    /// using the Breadth First Search and Uniform Cost Search algorithm</para>.
    /// </summary>
    public class ShortestPathsFinding
    {
        /// <value> If the value is true, the graph is weighted,
        /// otherwise the graph is unweighted.</value>
        private readonly bool Weighted;

        public Dictionary<string, Dictionary<string, double>> Children { get; set; }

        /// <summary>
        /// Initializes values.
        /// </summary>
        /// <param name="edgeList">A list of all graph edges.</param>
        /// <param name="directed">If the value is true, the graph is directed,
        /// otherwise the graph is undirected.</param>
        /// <param name="weighted">If the value is true, the graph is weighted,
        /// otherwise the graph is unweighted.</param>
        public ShortestPathsFinding(List<Edge> edgeList, bool directed, bool weighted)
        {
            this.Weighted = weighted;

            Children = new Dictionary<string, Dictionary<string, double>>();

            foreach (Edge edge in edgeList)
            {
                AddNodeToChildren(edge.Source);
                AddNodeToChildren(edge.Destination);
            }

            foreach (Edge edge in edgeList)
            {
                AddEdgeToChildren(edge);

                if (!directed)
                {
                    Edge reverseEdge = new Edge(edge.Destination, edge.Source, edge.Weight);
                    AddEdgeToChildren(reverseEdge);
                }
            }
        }

        /// <summary>
        /// Finds shortest paths between two nodes, 
        /// using the Breadth First Search and Uniform Cost Search algorithm
        /// </summary>
        /// <param name="source">The source node of the paths.</param>
        /// <param name="destination">The destination node of the paths.</param>
        /// <returns>Returns all edges of the shortest paths between 
        /// the source node and the destination node.</returns>
        public HashSet<Edge> Find(string source, string destination)
        {
            if (Weighted)
            {
                return UCS(source, destination);
            }
            return BFS(source, destination);
        }

        /// <summary>
        /// Finds unweighted shortest paths between two nodes, 
        /// using the Breadth First Search algorithm
        /// </summary>
        /// <param name="source">The source node of the paths.</param>
        /// <param name="destination">The destination node of the paths.</param>
        /// <returns>Returns all edges of the unweighted shortest paths between 
        /// the source node and the destination node.</returns>
        private HashSet<Edge> BFS(string source, string destination)
        {
            List<string> willBeExpanded = new List<string> { source };
            HashSet<string> checkedNodes = new HashSet<string> { source };
            Dictionary<string, string> parentOfNodes = new Dictionary<string, string> { { source, null } };
            HashSet<Edge> resultEdges = new HashSet<Edge>();

            do
            {
                List<string> newWillBeExpanded = new List<string>();

                foreach (string node in willBeExpanded)
                {
                    foreach (string child in Children[node].Keys.ToList())
                    {
                        if (!checkedNodes.Contains(child))
                        {
                            if (child == destination)
                            {
                                parentOfNodes.Add(child, node);
                                string n = child;
                                while (parentOfNodes[n] != null)
                                {
                                    resultEdges.Add(new Edge(parentOfNodes[n], n));
                                    n = parentOfNodes[n];
                                }
                                parentOfNodes.Remove(child);
                            }
                            else
                            {
                                parentOfNodes[child] = node;
                                checkedNodes.Add(child);
                                newWillBeExpanded.Add(child);
                            }
                        }
                    }
                }
                willBeExpanded = newWillBeExpanded;
                if (resultEdges.Count > 0)
                {
                    return resultEdges;
                }
            } while (willBeExpanded.Count > 0);

            return resultEdges;
        }

        /// <summary>
        /// Finds weighted shortest paths between two nodes, 
        /// using the Uniform Cost Search algorithm.
        /// </summary>
        /// <param name="source">The source node of the paths.</param>
        /// <param name="destination">The destination node of the paths.</param>
        /// <returns>Returns all edges of the weighted shortest paths between 
        /// the source node and the destination node.</returns>
        private HashSet<Edge> UCS(string source, string destination)
        {
            Dictionary<string, List<string>> parentsOfNodes = new Dictionary<string, List<string>>();
            Dictionary<string, double> costOfNodes = new Dictionary<string, double>();
            Dictionary<string, double> willBeExpanded = new Dictionary<string, double> { { source, 0 } };
            HashSet<Edge> resultEdges = new HashSet<Edge>();

            while (willBeExpanded.Count > 0)
            {
                string node = willBeExpanded.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
                if (costOfNodes.ContainsKey(destination) && costOfNodes[destination] < willBeExpanded[node])
                {
                    break;
                }
                Expand(node, costOfNodes, parentsOfNodes, willBeExpanded, source);
            }

            GetResultEdges(destination, new Path(new List<string> { destination }), source, resultEdges, parentsOfNodes);

            return resultEdges;
        }

        private void Expand(string node, Dictionary<string, double> costOfNodes,
            Dictionary<string, List<string>> parentsOfNodes,
            Dictionary<string, double> willBeExpanded, string source)
        {
            costOfNodes[node] = willBeExpanded[node];
            foreach (string child in Children[node].Keys.ToList())
            {
                if (child == source)
                {
                    continue;
                }
                double cost = costOfNodes[node] + Children[node][child];
                if (costOfNodes.ContainsKey(child))
                {
                    if (costOfNodes[child] == cost)
                    {
                        parentsOfNodes[child].Add(node);
                    }
                }
                else if (willBeExpanded.ContainsKey(child))
                {
                    if (willBeExpanded[child] > cost)
                    {
                        willBeExpanded[child] = cost;
                        parentsOfNodes[child].Clear();
                        parentsOfNodes[child].Add(node);
                    }
                    else if (willBeExpanded[child] == cost)
                    {
                        parentsOfNodes[child].Add(node);
                    }
                }
                else
                {
                    willBeExpanded.Add(child, cost);
                    parentsOfNodes[child] = new List<string> { node };
                }
            }
            willBeExpanded.Remove(node);
        }

        private void GetResultEdges(string node, Path path, string source, HashSet<Edge> result,
                Dictionary<string, List<string>> parents)
        {
            if (node == source)
            {
                for (int i = 0; i < path.Nodes.Count - 1; i++)
                {
                    result.Add(new Edge(path.Nodes[i + 1], path.Nodes[i]));
                }
            }
            else
            {
                if (parents.ContainsKey(node))
                {
                    foreach (string parent in parents[node])
                    {
                        if (!path.Nodes.Contains(parent))
                        {
                            Path newPath = new Path(new List<string>(path.Nodes));
                            newPath.Nodes.Add(parent);
                            GetResultEdges(parent, newPath, source, result, parents);
                        }
                    }
                }
            }
        }

        private void AddNodeToChildren(string node)
        {
            if (!Children.ContainsKey(node))
            {
                Children.Add(node, new Dictionary<string, double>());
            }
        }

        private void AddEdgeToChildren(Edge edge)
        {
            if (Children[edge.Source].ContainsKey(edge.Destination))
            {
                if (Children[edge.Source][edge.Destination] > edge.Weight)
                {
                    Children[edge.Source][edge.Destination] = edge.Weight;
                }
            }
            else
            {
                Children[edge.Source].Add(edge.Destination, edge.Weight);
            }
        }
    }
}
