﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.models
{
    /// <summary>
    /// The Path class
    /// <para>contains all nodes of the path.</para>
    /// </summary>
    public class Path
    {
        /// <value>Gets a list of all nodes of the path.</value>
        public List<string> Nodes { get; set; }

        /// <summary>
        /// A constructor for creating an edge.
        /// </summary>
        /// <param name="nodes">The nodes of the path.</param>
        public Path(List<string> nodes)
        {
            this.Nodes = nodes;
        }
    }
}
