﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding.models
{
    /// <summary>
    /// The PathsLength class
    /// <para>contains all paths with equal length.</para>
    /// </summary>
    public class PathsLength
    {
        /// <value>Gets all paths with equal length according to the destination node.</value>
        public Dictionary<string, List<Path>> PathsByDestinationId { get; set; }

        /// <summary>
        /// A constructor for creating all paths with equal length.
        /// </summary>
        /// <param name="pathsByDestinationId">All paths with equal length</param>
        public PathsLength(Dictionary<string, List<Path>> pathsByDestinationId)
        {
            this.PathsByDestinationId = pathsByDestinationId;
        }
    }
}
