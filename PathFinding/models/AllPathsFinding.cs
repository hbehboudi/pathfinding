﻿using ConcurrentCollections;
using PathFinding.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding
{
    /// <summary>
    /// The AllPathsFinding class
    /// <para>finds all possible paths with allowed lengths between two nodes, 
    /// using the Bidirectional Search algorithm</para>.
    /// </summary>
    public class AllPathsFinding
    {
        private Dictionary<string, List<string>> Children { get; set; }
        private Dictionary<string, List<string>> Parents { get; set; }

        /// <summary>
        /// Initializes values.
        /// </summary>
        /// <param name="edgeList">A list of all graph edges.</param>
        /// <param name="directed">If the value is true, the graph is directed,
        /// otherwise the graph is undirected.</param>
        public AllPathsFinding(List<Edge> edgeList, bool directed)
        {
            Children = new Dictionary<string, List<string>>();
            Parents = new Dictionary<string, List<string>>();

            foreach (Edge edge in edgeList)
            {
                AddNode(edge.Source, Children);
                AddNode(edge.Destination, Children);
                AddNode(edge.Source, Parents);
                AddNode(edge.Destination, Parents);

                AddEdge(edge);
                if (!directed)
                {
                    Edge reverse = new Edge(edge.Destination, edge.Source);
                    AddEdge(reverse);
                }
            }
        }

        /// <summary>
        /// finds all possible paths between two nodes with a permitted length, 
        /// using the Bidirectional Search algorithm.
        /// </summary>
        /// <param name="source">The source node of the paths.</param>
        /// <param name="destination">The destination node of the paths.</param>
        /// <param name="min">Minimum path length.</param>
        /// <param name="max">Maximum path length</param>
        /// <returns>Returns all edges of all paths between the source node and 
        /// the destination node with a permitted length.</returns>
        public ConcurrentHashSet<Edge> Find(string source, string destination, int min, int max)
        {
            ConcurrentHashSet<Edge> resultEdges = new ConcurrentHashSet<Edge>();

            List<PathsLength> sourceHalfPaths = GetHalfPaths(Children, source, (max + 1) / 2);
            List<PathsLength> destinationHalfPaths = GetHalfPaths(Parents, destination, max / 2);

            for (int i = min; i <= max; i++)
            {
                PathsLength sourcePathsLength = sourceHalfPaths[(i + 1) / 2];
                PathsLength destinationPathsLength = destinationHalfPaths[i / 2];

                Parallel.ForEach(destinationPathsLength.PathsByDestinationId.Keys, middleNode =>
                {
                    if (sourcePathsLength.PathsByDestinationId.ContainsKey(middleNode))
                    {
                        foreach (Path sourcePath in sourcePathsLength.PathsByDestinationId[middleNode])
                        {
                            foreach (Path destinationPath in destinationPathsLength.PathsByDestinationId[middleNode])
                            {
                                if (NumberOfCommonNodes(sourcePath, destinationPath) == 1)
                                {
                                    for (int j = 0; j < sourcePath.Nodes.Count - 1; j++)
                                    {
                                        resultEdges.Add(new Edge(sourcePath.Nodes[j], sourcePath.Nodes[j + 1]));
                                    }
                                    for (int j = 0; j < destinationPath.Nodes.Count - 1; j++)
                                    {
                                        resultEdges.Add(new Edge(destinationPath.Nodes[j + 1], destinationPath.Nodes[j]));
                                    }
                                }
                            }
                        }
                    }
                });
            }
            return resultEdges;
        }

        /// <summary>
        /// Finds half of the path from source to destination or destination to source.
        /// </summary>
        /// <param name="neighbours">The neighbors of each node.</param>
        /// <param name="source">The source node.</param>
        /// <param name="maxLength">Maximum path length</param>
        /// <returns>Returns half of all paths from source to destination or
        /// from destination to source.</returns>
        private List<PathsLength> GetHalfPaths(Dictionary<string, List<string>> neighbours, string source, int maxLength)
        {
            List<PathsLength> halfPaths = new List<PathsLength>
            {
                new PathsLength(new Dictionary<string, List<Path>>())
            };

            halfPaths[0].PathsByDestinationId.Add(source, new List<Path>());
            Path firstPath = new Path(new List<string>());
            firstPath.Nodes.Add(source);
            halfPaths[0].PathsByDestinationId[source].Add(firstPath);

            for (int i = 0; i < maxLength; i++)
            {
                halfPaths.Add(new PathsLength(new Dictionary<string, List<Path>>()));
                foreach (string destination in halfPaths[i].PathsByDestinationId.Keys)
                {
                    foreach (string nextNode in neighbours[destination])
                    {
                        if (!halfPaths[i + 1].PathsByDestinationId.ContainsKey(nextNode))
                        {
                            halfPaths[i + 1].PathsByDestinationId.Add(nextNode, new List<Path>());
                        }
                        foreach (Path path in halfPaths[i].PathsByDestinationId[destination])
                        {
                            if (!path.Nodes.Contains(nextNode))
                            {
                                Path newPath = new Path(new List<string>(path.Nodes));
                                newPath.Nodes.Add(nextNode);
                                halfPaths[i + 1].PathsByDestinationId[nextNode].Add(newPath);
                            }
                        }
                    }
                }
            }
            return halfPaths;
        }

        /// <summary>
        /// Gets the number of common nodes between the two paths.
        /// </summary>
        /// <param name="pathFromSource">A path from the source node to the middle node of the path.</param>
        /// <param name="pathToDestination">A path from the middle node of the path to the destination node.</param>
        /// <returns>Returns the number of common nodes between the two paths.</returns>
        private int NumberOfCommonNodes(Path path1, Path path2)
        {
            int num = 0;
            foreach (string node in path1.Nodes)
            {
                if (path2.Nodes.Contains(node))
                {
                    num++;
                }
            }
            return num;
        }
        
        private void AddNode(string node, Dictionary<string, List<string>> neighbours)
        {
            if (!neighbours.ContainsKey(node))
            {
                neighbours.Add(node, new List<string>());
            }
        }

        private void AddEdge(Edge edge)
        {
            if (!Children[edge.Source].Contains(edge.Destination))
            {
                Children[edge.Source].Add(edge.Destination);
                Parents[edge.Destination].Add(edge.Source);
            }
        }
    }
}
