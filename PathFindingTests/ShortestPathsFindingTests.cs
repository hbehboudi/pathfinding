﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PathFinding;
using PathFinding.models;
using System.Linq;

namespace PathFindingTests
{
    [TestClass]
    public class ShortestPathsFindingTests
    {
        [TestMethod()]
        public void ShortestPathDirected1()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("1", "5", 3),
                new Edge("5", "4", 3)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, true, false);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "5")));
                Assert.IsTrue(paths.Contains(new Edge("5", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathDirected2()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("4", "5", 1),
                new Edge("1", "7", 3),
                new Edge("7", "5", 3)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, true, false);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 3);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathDirected3()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, true, false);
            List<Edge> paths = shortestPath.Find("3", "1").ToList();

            Assert.IsTrue(paths.Count == 0);
        }

        [TestMethod()]
        public void ShortestPath1()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("4", "5", 1),
                new Edge("5", "6", 1),
                new Edge("1", "7", 3),
                new Edge("7", "6", 3)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, false);
            List<Edge> paths = shortestPath.Find("1", "6").ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "7")));
                Assert.IsTrue(paths.Contains(new Edge("7", "6")));
            }
        }

        [TestMethod()]
        public void ShortestPath2()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("4", "5", 1),
                new Edge("1", "7", 3),
                new Edge("7", "5", 3)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, false);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 6);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "7")));
                Assert.IsTrue(paths.Contains(new Edge("7", "5")));
                Assert.IsTrue(paths.Contains(new Edge("5", "4")));
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight1()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("1", "4", 1)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 1);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight2()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("1", "4", 4)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 3);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight3()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("1", "3", 1),
                new Edge("1", "4", 3)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight4()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 3),
                new Edge("2", "3", 2),
                new Edge("3", "2", 1),
                new Edge("1", "3", 1)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "2").ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "2")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight5()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 3),
                new Edge("2", "3", 3),
                new Edge("3", "2", 1),
                new Edge("1", "3", 5)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "3").ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight6()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 3),
                new Edge("2", "3", 3),
                new Edge("3", "2", 1),
                new Edge("1", "3", 5)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 0);
        }

        [TestMethod()]
        public void ShortestPathWeight7()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("1", "3", 2)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "3").ToList();

            Assert.IsTrue(paths.Count == 3);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight8()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("1", "3", 2),
                new Edge("3", "4", 0)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "4").ToList();

            Assert.IsTrue(paths.Count == 4);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeight9()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 1),
                new Edge("2", "3", 1),
                new Edge("3", "4", 1),
                new Edge("4", "5", 1),
                new Edge("1", "3", 2),
                new Edge("1", "4", 3),
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, false, true);
            List<Edge> paths = shortestPath.Find("1", "5").ToList();

            Assert.IsTrue(paths.Count == 6);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "4")));
                Assert.IsTrue(paths.Contains(new Edge("4", "5")));
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
                Assert.IsTrue(paths.Contains(new Edge("1", "4")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeightDirected1()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2", 3),
                new Edge("2", "3", 3),
                new Edge("3", "2", 1),
                new Edge("1", "3", 3)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, true, true);
            List<Edge> paths = shortestPath.Find("1", "3").ToList();

            Assert.IsTrue(paths.Count == 1);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
            }
        }

        [TestMethod()]
        public void ShortestPathWeightDirected2()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "4", 1),
                new Edge("1", "2", 3),
                new Edge("4", "2", 1),
                new Edge("2", "5", 2),
                new Edge("2", "3", 1),
                new Edge("5", "6", 1),
                new Edge("3", "6", 2)
            };

            ShortestPathsFinding shortestPath = new ShortestPathsFinding(edges, true, true);
            List<Edge> paths = shortestPath.Find("1", "6").ToList();

            Assert.IsTrue(paths.Count == 6);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "4")));
                Assert.IsTrue(paths.Contains(new Edge("4", "2")));
                Assert.IsTrue(paths.Contains(new Edge("2", "5")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("5", "6")));
                Assert.IsTrue(paths.Contains(new Edge("3", "6")));
            }
        }
    }
}
