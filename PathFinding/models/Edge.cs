﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PathFinding
{
    /// <summary>
    /// The Edge class
    /// <para>contains source and destination and weight of the edge.</para>
    /// </summary>
    public class Edge
    {
        /// <value>Gets the source node of the edge.</value>
        public string Source { get; set; }

        /// <value>Gets the destination node of the edge.</value>
        public string Destination { get; set; }

        /// <value>Gets the weight of the edge.</value>
        public double Weight { get; set; }

        /// <summary>
        /// A constructor for creating an edge.
        /// </summary>
        /// <param name="source">A source node.</param>
        /// <param name="destination">A destination node.</param>
        /// <param name="weight">A weight number.</param>
        public Edge(string source, string destination, double weight)
        {
            this.Source = source;
            this.Destination = destination;
            this.Weight = weight;
        }
        
        /// <summary>
        /// A constructor for creating an edge.
        /// </summary>
        /// <param name="source">A source node.</param>
        /// <param name="destination">A destination node.</param>
        public Edge(string source, string destination)
        {
            this.Source = source;
            this.Destination = destination;
        }

        /// <summary>
        /// If the source and the destination of the edges are equal, they are equal.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>If they are equal, returns true value, otherwise it returns false value.</returns>
        public override bool Equals(object obj)
        {
            return (obj is Edge) && (this.Source == ((Edge)obj).Source) && (this.Destination == ((Edge)obj).Destination);
        }

        /// <summary>
        /// According to source and destination, it gives hash value.
        /// </summary>
        /// <returns>The HashCode of the edge.</returns>
        public override int GetHashCode()
        {
            return (Source.GetHashCode() + Destination.GetHashCode()) / 2;
        }
    }
}
