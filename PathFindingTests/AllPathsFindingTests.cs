﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PathFinding;
using System.Linq;

namespace PathFindingTests
{
    [TestClass]
    public class AllPathsFindingTests
    {
        [TestMethod()]
        public void AllPathDirected1()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2"),
                new Edge("1", "3"),
                new Edge("2", "3"),
                new Edge("3", "4"),
                new Edge("3", "5"),
                new Edge("4", "5"),
            };

            AllPathsFinding allPaths = new AllPathsFinding(edges, true);

            List<Edge> paths = allPaths.Find("1", "5", 3, 3).ToList();

            Assert.IsTrue(paths.Count == 6);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "2")));
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
                Assert.IsTrue(paths.Contains(new Edge("2", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "4")));
                Assert.IsTrue(paths.Contains(new Edge("3", "5")));
                Assert.IsTrue(paths.Contains(new Edge("4", "5")));
            }
        }

        [TestMethod()]
        public void AllPathDirected2()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2"),
                new Edge("1", "3"),
                new Edge("2", "3"),
                new Edge("3", "4"),
                new Edge("3", "5"),
                new Edge("4", "5"),
            };

            AllPathsFinding allPaths = new AllPathsFinding(edges, true);

            List<Edge> paths = allPaths.Find("1", "5", 2, 2).ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "5")));
            }
        }

        [TestMethod()]
        public void AllPathDirected3()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2" , 5),
                new Edge("1", "3" , 1),
                new Edge("3", "4" , 1),
                new Edge("4", "5" , 1),
                new Edge("1", "4" , 2),
            };

            AllPathsFinding allPaths = new AllPathsFinding(edges, true);

            List<Edge> paths = allPaths.Find("1", "5", 1, 2).ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("1", "4")));
                Assert.IsTrue(paths.Contains(new Edge("4", "5")));
            }
        }

        [TestMethod()]
        public void AllPath1()
        {
            List<Edge> edges = new List<Edge>
            {
                new Edge("1", "2"),
                new Edge("1", "3"),
                new Edge("2", "3"),
                new Edge("3", "4"),
                new Edge("3", "5"),
                new Edge("4", "5"),
            };

            AllPathsFinding allPaths = new AllPathsFinding(edges, false);

            List<Edge> paths = allPaths.Find("5", "1", 2, 2).ToList();

            Assert.IsTrue(paths.Count == 2);
            foreach (Edge edge in paths)
            {
                Assert.IsTrue(paths.Contains(new Edge("5", "3")));
                Assert.IsTrue(paths.Contains(new Edge("3", "1")));
            }
        }
    }
}
